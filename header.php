<?php
    include "db.php";
    include "regdb.php";
    include "apinfo.php";

    $plural_singular_def = "as";
    $day_of_year = (int) date('z');
    $plural_singular_set = $day_of_year % 10;

    if ($plural_singular_set === 1) {
        $plural_singular_def = "a";
    };

    $daysleft = (365 - $day_of_year)/7;
    $weeksleft = number_format($daysleft, 0, '.', '');
    $week_plural_singular_def = "as";
    if ($weeksleft == 1) {
        $week_plural_singular_def = "a";
    }

    // lokala IP adrese / klients
    $localIP = getHostByName(getHostName());
    $client = php_sapi_name();

?>
<link rel="stylesheet" href="style.css">
