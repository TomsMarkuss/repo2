<!DOCTYPE html>
<html>
  <head>
    <?php
      include "header.php";
      if(isset($_COOKIE['truck'])) {
        header('location: index.php');
      }
    ?>
    <Style></Style>
  </head>
  <body>
    <form action="login.php" method="post" id="register_form">
      <h1>LOGIN</h1>
      <div class="container">
        <div <?php if (isset($uname_error)): ?> class="form_error" <?php endif ?> >
          <input type="text" placeholder="Enter Username" name="reusername" required>
          <?php if (isset($uname_error)): ?>
          <span><?php echo $uname_error; ?></span>
          <?php endif ?>
        </div>
        <div<?php if (isset($psw_error)): ?> class="form_error" <?php endif ?> >
          <input type="password" placeholder="Enter Password" name="repsw" required>
          <?php if (isset($psw_error)): ?>
          <span><?php echo $psw_error; ?></span>
          <?php endif ?>
        </div>
        <div>
          <button type="submit" name="login" id="reg_btn">Login</button>
        </div>
      </div>
    </form>
  </body>
</html>
