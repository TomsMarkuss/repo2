<!DOCTYPE html nemu nost tev admin tiesības>
<html>
	<head>
		<link type="text/css" href="redesign.css" rel="stylesheet" />
		
		<?php
			include "header.php";
			if(!isset($_COOKIE['truck'])) {
				header('location: login.php');
			}
		?>
		<style></style>
	</head>
	<body>
		<div>
		Čau, amigo!
		
		Šodien ir pagājušas <?php echo $day_of_year;?> dien<?php echo $plural_singular_def;?> kopš gada sākuma, kad tu sev uzstādīji šādus mērķus:
		<?php 
			
			// page brake in console and in browser
			if ($client == "cli") {
				echo PHP_EOL;
			} else { 
				echo '<br />';
				echo '<br />';
			}
			
			$values = array();
			// Goal display from DB
			while($row = mysqli_fetch_assoc($result)) {
				$ID = $row['id'];
				$status = $row['status'];
				
				echo '- ';
				echo $row["goal"];

				if ($row['status']) {
					echo ' <a href="?upstat=0&id='.$ID.'">DONE</a>';
				} else {
					echo ' <a href="?upstat=1&id='.$ID.'">NOT DONE</a>';
				}
				echo ' <a href="?delete='.$ID.'">DZEST</a>';
				
				if ($client == "cli") {
					echo PHP_EOL;
				} else { 
					echo '<br />';
				}
				
						
			}
			?>
			<a href='addgoal.php'>ADD</a>
			<?php
			//page brake in console and in browser
			if ($client == "cli") {
				echo PHP_EOL;
			} else { 
				echo '<br />';
			}
			mysqli_close($conn);
			
		?>


		Pēc pēdējām atskaitēm redzu, ka vēl atmest smēķēšanu nav vēl izdevies.
		Tev ir palikušas vēl <?php echo $weeksleft;?> nedēļ<?php echo $week_plural_singular_def;?> to izdarīt.

		Šobrīd pasaulē ir jau <?php echo number_format($covidcases);?> koronavīrusa saslimšanas gadījumi. Smēķētāji esot riska grupā.
		Ja no šodienas samazināsi cigarešu patēriņu par 1 cigareti dienā, būsi atmetis jau pēc mēneša.


		<?php echo $data1['country'];?> tuvojas arī ziema. Jau šobrīd gaisa temperatūra nokritusies līdz <?php echo($weatherdata['temp']);?>^C. Naktis kļūst garākas - Saule aust <?php echo date('H:i',$sunrise);?>, bet riet jau <?php echo date('H:i',$sunset);?>.

		Tavā facebook profila bildē redzu divus cilvēkus, bet neredzu kailumu...
		Huļi tu vispār laiko SamsungLV lapu facebookā?

		Tev šodien nav ne vārda diena, ne dzimšanas diena, tāpēc atceries apsveikt visus Jāņus un Antonus.
		</div>
		<?php include "footer.php";?>
	</body>
</html>
