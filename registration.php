<!DOCTYPE html>
<html>
  <head>
    <?php
      include "header.php";
    ?>
    <Style></Style>
  </head>
    <body>
      <form action="registration.php" method="post" id="register_form">
        <h1>REGISTRATION</h1>	
        <div class="container">
          <div>
            <input type="text" placeholder="Enter Name" name="name" required>
          </div>
          <div>
            <input type="text" placeholder="Enter Surname" name="surname" required>
          </div>
          <div <?php if (isset($email_error)): ?> class="form_error" <?php endif ?> >
            <input type="email" placeholder="Enter email" name="email" value="<?php echo $email; ?>" required>
            <?php if (isset($email_error)): ?>
              <span><?php echo $email_error; ?></span>
            <?php endif ?>
          </div>
          <div>
            <input type="tel" placeholder="Enter Phone number" name="phone" required>
          </div>
          <div <?php if (isset($name_error)): ?> class="form_error" <?php endif ?> >
            <input type="text" placeholder="Enter Username" name="username" value="<?php echo $username; ?>" required>
            <?php if (isset($name_error)): ?>
              <span><?php echo $name_error; ?></span>
            <?php endif ?>
          </div>
          <div>
            <input type="password" placeholder="Enter Password" name="psw" required>
          </div>
          <div>
            <input type="password" placeholder="Confirm Password" name="cpsw"  id="confirm_password" required>
          </div>
          <div>
            <button type="submit" name="register" id="reg_btn">Register</button>
          </div>
        </div>
      </form>
    </body>
  <?php 
  include "footer.php";
  ?>
</html>
